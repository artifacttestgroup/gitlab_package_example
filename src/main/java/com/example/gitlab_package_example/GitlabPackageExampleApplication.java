package com.example.gitlab_package_example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabPackageExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabPackageExampleApplication.class, args);
    }

}
